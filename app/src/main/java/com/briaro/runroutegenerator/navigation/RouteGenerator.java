package com.briaro.runroutegenerator.navigation;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.briaro.runroutegenerator.Constants;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

public class RouteGenerator {
    private final RoadManger roadManger;
    private final Context context;

    public RouteGenerator(RoadManger roadManger, Context context) {
        this.roadManger = roadManger;
        this.context = context;
    }

    // Returns routed distance in meters.
    public static double calculateRouteDistance(ArrayList<RoadPoint> route) {
        if (route.size() < 2)
            return 0;

        double routedDistance = 0;
        for (int i = 0; i <= route.size() - 2; i++) {
            routedDistance += route.get(i).getDistanceTo(route.get(i + 1));
        }

        return routedDistance;
    }

    public ArrayList<RoadPoint> generateRoute(GeoPoint startGeoPoint, double totalDistanceMeters) {
        roadManger.fetchRoads(startGeoPoint, totalDistanceMeters);

        displayToast("Generating route.");
        GeoPoint closestRoadPoint = roadManger.getClosestPointOnRoad(startGeoPoint);
        if (closestRoadPoint == null)
            return new ArrayList<>();

        RoadPoint startPoint = new RoadPoint(closestRoadPoint);
        roadManger.addStartPoint(startPoint);
        ArrayList<RoadPoint> route = generateRouteLoop(startPoint, totalDistanceMeters);
        roadManger.deleteStartPoint();
        displayToast("Saving road data.");
        roadManger.saveRoadPoints();
        displayToast("Finished.");
        return route;
    }

    private void displayToast(final String message) {
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    // Returns a list of the RoadPoints that were traveled the longest ago.
    private ArrayList<RoadPoint> findOldestTraveled(Collection<RoadPoint> roadPoints) {
        long oldestTime = Long.MAX_VALUE;
        for (RoadPoint roadPoint : roadPoints) {
            long lastTraveledTime = roadPoint.getLastTraveled().getTime();
            if (lastTraveledTime < oldestTime)
                oldestTime = lastTraveledTime;
        }

        ArrayList<RoadPoint> oldestPoints = new ArrayList<>();
        for (RoadPoint roadPoint : roadPoints) {
            if (roadPoint.getLastTraveled().getTime() == oldestTime)
                oldestPoints.add(roadPoint);
        }

        return oldestPoints;
    }

    // From roadPoints, find closest point that's routed distance is greater than distance.  If no
    // roadPoint is found that is over distance, then the farthest point is return.
    private RoadPoint findShortestOver(ArrayList<RoadPoint> roadPoints, double distance) {
        if (roadPoints.size() == 0)
            return null;

        RoadPoint farthestPoint = roadPoints.get(0);
        RoadPoint closestPointOver = null;
        for (RoadPoint roadPoint : roadPoints) {
            if (roadPoint.getRoutedDistance() > farthestPoint.getRoutedDistance())
                farthestPoint = roadPoint;

            if (roadPoint.getRoutedDistance() > distance) {
                if (closestPointOver == null)
                    closestPointOver = roadPoint;
                else if (roadPoint.getRoutedDistance() < closestPointOver.getRoutedDistance())
                    closestPointOver = roadPoint;
            }
        }

        return closestPointOver == null ? farthestPoint : closestPointOver;
    }

    // This assumes that the RoadPoint's routedDistance is updated in updateRoutedDistances().
    private ArrayList<RoadPoint> findShortestRoute(RoadPoint startPoint, RoadPoint endPoint) {
        ArrayList<RoadPoint> route = new ArrayList<>();
        route.add(endPoint);
        if (endPoint == startPoint)
            return route;

        RoadPoint bestConnection = endPoint.getBestConnection();
        while ((bestConnection != null) && (bestConnection != startPoint)) {
            route.add(bestConnection);
            bestConnection = bestConnection.getBestConnection();
        }

        route.add(startPoint);
        Collections.reverse(route);
        return route;
    }

    @NonNull
    private ArrayList<RoadPoint> generateRouteLoop(RoadPoint startPoint, double totalDistanceMeters) {
        double currentOutDistance = 0;
        double returnRouteDistance = 0;
        RoadPoint nextStartPoint = startPoint;
        ArrayList<RoadPoint> route = new ArrayList<>();
        ArrayList<RoadPoint> returnRoute = new ArrayList<>();
        while ((currentOutDistance + returnRouteDistance) < totalDistanceMeters) {
            double remainingDistance = totalDistanceMeters - currentOutDistance;
            double remainingOutDistance = (remainingDistance - returnRouteDistance) / 2;
            Collection<RoadPoint> routablePoints = updateRoutedDistances(nextStartPoint, remainingOutDistance);
            ArrayList<RoadPoint> oldestTraveledPoints = findOldestTraveled(routablePoints);
            RoadPoint routeToPoint = findShortestOver(oldestTraveledPoints, remainingOutDistance);
            if (routeToPoint == null)
                break;

            route.addAll(findShortestRoute(nextStartPoint, routeToPoint));
            updateLastTraveled(route);
            currentOutDistance = calculateRouteDistance(route);
            toastRouteDistance(currentOutDistance);

            // Find return route
            updateRoutedDistances(routeToPoint, currentOutDistance);
            returnRoute = findShortestRoute(routeToPoint, startPoint);
            returnRouteDistance = calculateRouteDistance(returnRoute);

            nextStartPoint = routeToPoint;
        }

        route.addAll(returnRoute);
        updateLastTraveled(route);
        return route;
    }

    private void toastRouteDistance(double currentOutDistance) {
        double outDistanceMiles = currentOutDistance / Constants.metersPerMile;
        Locale locale = Locale.getDefault();
        String format = "%.2f";
        final String routedDistanceString = String.format(locale, format, outDistanceMiles);
        displayToast("Routed " + routedDistanceString + " miles so far...");
    }

    private void updateConnections(RoadPoint start, double range, Hashtable<GeoPoint, RoadPoint> routablePoints) {
        double startRoutedDistance = start.getRoutedDistance();
        for (RoadSegment connection : start.getConnections()) {
            if (connection.isBlocked())
                continue;

            double connectionRoutedDistance = startRoutedDistance + connection.getLengthMeters();
            RoadPoint connectionEndPoint = connection.getOtherEnd(start);
            if (connectionRoutedDistance < connectionEndPoint.getRoutedDistance()) {
                connectionEndPoint.setRoutedDistance(connectionRoutedDistance);
                routablePoints.put(connectionEndPoint.getLocation(), connectionEndPoint);
                if (connectionRoutedDistance < range)
                    updateConnections(connectionEndPoint, range, routablePoints);
            }
        }
    }

    private void updateLastTraveled(ArrayList<RoadPoint> roadPoints) {
        Date now = new Date();
        for (RoadPoint roadPoint : roadPoints) {
            roadPoint.setLastTraveled(now);
        }
    }

    // Returns all RoadPoints that can be routed to within the range specified.
    private Collection<RoadPoint> updateRoutedDistances(RoadPoint startPoint, double range) {
        roadManger.resetRoutedDistances();
        startPoint.setRoutedDistance(0);
        Hashtable<GeoPoint, RoadPoint> routablePointsTable = new Hashtable<>();
        updateConnections(startPoint, range, routablePointsTable);
        return routablePointsTable.values();
    }
}
