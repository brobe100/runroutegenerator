package com.briaro.runroutegenerator.navigation;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.Date;

public class RoadPoint {
    private final GeoPoint location;
    private final ArrayList<RoadSegment> connections = new ArrayList<>();
    private String roadName = "";
    private Date lastTraveled = new Date(0);

    // routedDistance is for use by the routing algorithm.  It is the shortest routed distance from
    // the start point.
    private double routedDistance = Double.MAX_VALUE;

    public RoadPoint(GeoPoint location) {
        this.location = location;
    }

    public RoadPoint(GeoPoint location, String roadName) {
        this.location = location;
        this.roadName = roadName;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        if (obj.getClass() != getClass())
            return false;

        final RoadPoint rhs = (RoadPoint) obj;
        return this.location == rhs.location && this.connections == rhs.connections &&
                this.roadName.equals(rhs.roadName) && this.lastTraveled == rhs.lastTraveled;

    }

    // Returns the connection with the lowest routedDistance.
    public RoadPoint getBestConnection() {
        if (connections.size() == 0)
            return null;

        RoadPoint bestConnectionPoint = null;
        double lowestRoutedDistance = Double.MAX_VALUE;
        for (RoadSegment connection : connections) {
            if (connection.isBlocked())
                continue;

            RoadPoint connectionEndPoint = connection.getOtherEnd(this);
            if (connectionEndPoint.routedDistance < lowestRoutedDistance) {
                bestConnectionPoint = connectionEndPoint;
                lowestRoutedDistance = bestConnectionPoint.routedDistance;
            }
        }

        return bestConnectionPoint;
    }

    public void resetRoutedDistance() {
        routedDistance = Double.MAX_VALUE;
    }

    public double getRoutedDistance() {
        return routedDistance;
    }

    public void setRoutedDistance(double routedDistance) {
        this.routedDistance = routedDistance;
    }

    public RoadSegment addConnection(RoadPoint connectionRoadPoint) {
        // Don't add connection to self.
        if (connectionRoadPoint.getLocation() == this.getLocation())
            return null;

        // If connection already exist, don't add it again.
        for (RoadSegment connection : connections) {
            if (connection.getOtherEnd(this) == connectionRoadPoint)
                return connection;
        }

        // Create new connection, and add it to the list.
        RoadSegment newConnection = new RoadSegment(this, connectionRoadPoint);
        connections.add(newConnection);

        // Add the new connection to the connecting RoadPoints connection list.
        connectionRoadPoint.addConnection(newConnection);

        return newConnection;
    }

    public void deleteConnection(RoadPoint deletedConnectionPoint) {
        for (RoadSegment connection : connections) {
            if (connection.getOtherEnd(this) == deletedConnectionPoint) {
                connections.remove(connection);
                return;
            }
        }
    }

    public ArrayList<RoadSegment> getConnections() {
        return connections;
    }

    public double getDistanceTo(RoadPoint nextPoint) {
        return this.getLocation().distanceToAsDouble(nextPoint.getLocation());
    }

    public GeoPoint getLocation() {
        return location;
    }

    public Date getLastTraveled() {
        return lastTraveled;
    }

    public void setLastTraveled(Date lastTraveled) {
        this.lastTraveled = lastTraveled;
    }

    // Returns true if this road point should be saved to cache.
    public boolean shouldBeSaved() {
        return (lastTraveled.getTime() > 0);
    }

    private void addConnection(RoadSegment newConnection) {
        connections.add(newConnection);
    }
}
