// This class handles storing road data loading road data, and some helper functions.
// Note that roads consist of RoadPoints and RoadSegments.  A RoadSegment consist of two RoadPoints.
// If two RoadSegments connect, then they share a common RoadPoint.  RoadPoints also contain a
// reference to any RoadSegments that reference them.
//
// Note that fetchRoads should be called to download an area of roads first.

package com.briaro.runroutegenerator.navigation;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import org.osmdroid.bonuspack.kml.KmlFeature;
import org.osmdroid.bonuspack.kml.KmlFolder;
import org.osmdroid.bonuspack.kml.KmlPlacemark;
import org.osmdroid.bonuspack.location.OverpassAPIProvider;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.Distance;
import org.osmdroid.util.GeoPoint;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;

import static java.lang.Math.max;

public class RoadManger {
    private static final String ROAD_POINTS_FILE = "road_points_data";
    private static final String ROAD_POINTS_FILE_VERSION = "0.3";
    private static final String BLOCKED_ROADS_FILE = "blocked_roads";
    private static final String BLOCKED_ROADS_FILE_VERSION = "0.1";
    private final Context context;
    private final HashMap<GeoPoint, RoadPoint> roadPoints = new HashMap<>();
    private RoadPoint startPoint;
    private ArrayList<BoundingBox> fetchedAreas = new ArrayList<>();

    public RoadManger(Context context, FinishedLoadingListener finishedLoadingListener) {
        this.context = context;
        new LoadRoadsThread(finishedLoadingListener).start();
    }

    public ArrayList<RoadSegment> getBlockedSegments() {
        ArrayList<RoadSegment> blockedRoadSegments = new ArrayList<>();
        for (RoadPoint roadPoint : roadPoints.values()) {
            for (RoadSegment connection : roadPoint.getConnections()) {
                if (connection.isBlocked())
                    blockedRoadSegments.add(connection);
            }
        }

        return blockedRoadSegments;
    }

    public void blockRoadSegment(GeoPoint location, boolean block) {
        getClosestRoadSegment(location).setBlocked(block);
        saveBlockedRoads();
    }

    public void saveRoadPoints() {
        displayToast("Saving road points data.");
        try {
            File file = new File(context.getCacheDir(), ROAD_POINTS_FILE);
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(getRoadPointsData().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        displayToast("Finished saving road points data.");
    }

    // This resets all the roadPoints routedDistance.  This is called by the routing algorithm
    // before it begins calculating a new route.
    public void resetRoutedDistances() {
        startPoint.resetRoutedDistance();
        for (RoadPoint roadPoint : roadPoints.values()) {
            roadPoint.resetRoutedDistance();
        }
    }

    // Make sure to fetchRoads first to pull road data from map server.
    public void fetchRoads(GeoPoint areaCenter, double areaWidthInMeters) {
        BoundingBox area = getBoundingBox(areaCenter, areaWidthInMeters);
        if (hasBeenFetched(area))
            return;

        displayToast("Fetching roads.");
        OverpassAPIProvider overpassProvider = new OverpassAPIProvider();
        KmlFolder fetchedData = new KmlFolder();
        BoundingBox boundingBox = getBoundingBox(areaCenter, max(areaWidthInMeters, 500));
        String url = buildFetchUrl(boundingBox);
        overpassProvider.addInKmlFolder(fetchedData, url);
        extractRoadData(fetchedData);
        addFetchedArea(area);
    }

    public GeoPoint getClosestPointOnRoad(GeoPoint position) {
        double minDistance = Double.MAX_VALUE;
        GeoPoint closestPoint = null;
        for (RoadPoint roadPoint : roadPoints.values()) {
            for (RoadSegment connection : roadPoint.getConnections()) {
                double distance = getDistanceToSegment(position, connection);
                if (distance < minDistance) {
                    minDistance = distance;
                    closestPoint = getClosestPointOnSegment(position, connection);
                }
            }
        }

        return closestPoint;
    }

    // Inserts the road point into the closest road segment.
    public void addStartPoint(RoadPoint pointToInsert) {
        RoadSegment closestRoadSegment = getClosestRoadSegment(pointToInsert.getLocation());
        if (closestRoadSegment != null)
            insertStartPoint(pointToInsert, closestRoadSegment);
    }

    // Deletes a RoadPoint from RoadManger, and reconnects the surrounding RoadPoints.
    public void deleteStartPoint() {
        ArrayList<RoadSegment> startPointConnections = startPoint.getConnections();
        for (RoadSegment startPointConnection : startPointConnections) {
            RoadPoint startConnectionPoint = startPointConnection.getOtherEnd(startPoint);
            startConnectionPoint.deleteConnection(startPoint);
            for (RoadSegment nextConnection : startPointConnections) {
                startConnectionPoint.addConnection(nextConnection.getOtherEnd(startPoint));
            }
        }
    }

    // This just adds the the fetched area to the fetchedAreas list.  Call this after the area has
    // been fetched.
    private void addFetchedArea(BoundingBox newArea) {
        removeInsideAreas(newArea);
        fetchedAreas.add(newArea);
    }

    private void addRoad(KmlPlacemark road) {
        String roadName = road.mName;
        ArrayList<GeoPoint> roadCoordinates = road.mGeometry.mCoordinates;
        if (roadCoordinates == null)
            return;

        for (int index = 0; index < roadCoordinates.size(); ++index) {
            GeoPoint currentCoordinate = roadCoordinates.get(index);
            if (!roadPoints.containsKey(currentCoordinate))
                roadPoints.put(currentCoordinate, new RoadPoint(currentCoordinate, roadName));

            if (index > 0) {
                RoadPoint previousPoint = roadPoints.get(roadCoordinates.get(index - 1));
                RoadPoint currentPoint = roadPoints.get(currentCoordinate);
                previousPoint.addConnection(currentPoint);
                currentPoint.addConnection(previousPoint);
            }
        }
    }

    private String buildFetchUrl(BoundingBox fetchArea) {
        StringBuilder url = new StringBuilder();
        url.append("http://overpass-api.de/api/interpreter" + "?data=");
        String fetchAreaString = "(" + fetchArea.getLatSouth() + "," + fetchArea.getLonWest() + ",";
        fetchAreaString += fetchArea.getLatNorth() + "," + fetchArea.getLonEast() + ")";

        String data = "[out:json][timeout:25];" +
                "(" +
                "way[\"highway\"]" +
                "[\"foot\"!=\"no\"]" +
                "[\"highway\"!=\"motorway_link\"]" +
                fetchAreaString + ";" +
                ");" +
                "out qt geom tags;";

        try {
            url.append(URLEncoder.encode(data, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }

        return url.toString();
    }

    private void displayToast(final String message) {
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void extractRoadData(KmlFolder fetchedData) {
        displayToast("Parsing road data.");
        int count = 1;
        int numberOfItems = fetchedData.mItems.size();
        for (KmlFeature feature : fetchedData.mItems) {
            displayToast("Parsing " + count++ + " of " + numberOfItems + " roads.");
            if (feature instanceof KmlPlacemark)
                addRoad((KmlPlacemark) feature);
        }
    }

    private String getBlockedRoadsData() {
        HashSet<RoadSegment> blockedRoadSegments = new HashSet<>();
        for (RoadPoint roadPoint : roadPoints.values()) {
            for (RoadSegment roadSegment : roadPoint.getConnections()) {
                if (roadSegment.isBlocked())
                    blockedRoadSegments.add(roadSegment);
            }
        }

        StringBuilder blockedRoadsData = new StringBuilder();
        blockedRoadsData.append(BLOCKED_ROADS_FILE_VERSION + ' ');
        for (RoadSegment roadSegment : blockedRoadSegments) {
            blockedRoadsData.append(roadSegment.getFirstPoint().getLocation().getLatitude());
            blockedRoadsData.append(' ');
            blockedRoadsData.append(roadSegment.getFirstPoint().getLocation().getLongitude());
            blockedRoadsData.append(' ');
            blockedRoadsData.append(roadSegment.getSecondPoint().getLocation().getLatitude());
            blockedRoadsData.append(' ');
            blockedRoadsData.append(roadSegment.getSecondPoint().getLocation().getLongitude());
            blockedRoadsData.append(' ');
        }

        return blockedRoadsData.toString();
    }

    @NonNull
    private BoundingBox getBoundingBox(GeoPoint center, double widthInMeters) {
        double distance = widthInMeters / 2;
        double north = center.destinationPoint(distance, 0).getLatitude();
        double east = center.destinationPoint(distance, 90).getLongitude();
        double south = center.destinationPoint(distance, 180).getLatitude();
        double west = center.destinationPoint(distance, 270).getLongitude();
        return new BoundingBox(north, east, south, west);
    }

    private GeoPoint getClosestPointOnSegment(GeoPoint point, RoadSegment roadSegment) {
        GeoPoint start = roadSegment.getFirstPoint().getLocation();
        GeoPoint end = roadSegment.getSecondPoint().getLocation();
        double deltaLongitude = end.getLongitude() - start.getLongitude();
        double deltaLatitude = end.getLatitude() - start.getLatitude();
        if ((deltaLongitude == 0) && (deltaLatitude == 0))
            return new GeoPoint(start.getLongitude(), start.getLatitude());

        double v = (point.getLongitude() - start.getLongitude()) * deltaLongitude;
        double v1 = (point.getLatitude() - start.getLatitude()) * deltaLatitude;
        double segmentSquared = deltaLongitude * deltaLongitude + deltaLatitude * deltaLatitude;
        double u = (v + v1) / segmentSquared;
        if (u < 0)
            return new GeoPoint(start.getLatitude(), start.getLongitude());
        else if (u > 1)
            return new GeoPoint(end.getLatitude(), end.getLongitude());

        double closestLatitude = start.getLatitude() + u * deltaLatitude;
        double closestLongitude = start.getLongitude() + u * deltaLongitude;
        return new GeoPoint(closestLatitude, closestLongitude);
    }

    private RoadSegment getClosestRoadSegment(GeoPoint position) {
        double minDistance = Double.MAX_VALUE;
        RoadSegment closestRoadSegment = null;
        for (RoadPoint roadPoint : roadPoints.values()) {
            for (RoadSegment connection : roadPoint.getConnections()) {
                double distance = getDistanceToSegment(position, connection);
                if (distance < minDistance) {
                    minDistance = distance;
                    closestRoadSegment = connection;
                }
            }
        }

        return closestRoadSegment;
    }

    @NonNull
    private Double getDistanceToSegment(GeoPoint position, RoadSegment roadSegment) {
        Double positionLongitude = position.getLongitude();
        Double positionLatitude = position.getLatitude();
        GeoPoint segmentStartLocation = roadSegment.getFirstPoint().getLocation();
        Double startLongitude = segmentStartLocation.getLongitude();
        Double startLatitude = segmentStartLocation.getLatitude();
        GeoPoint segmentEndLocation = roadSegment.getSecondPoint().getLocation();
        Double endLongitude = segmentEndLocation.getLongitude();
        Double endLatitude = segmentEndLocation.getLatitude();
        Double distance;
        distance = Distance.getSquaredDistanceToSegment(positionLongitude, positionLatitude,
                startLongitude, startLatitude, endLongitude, endLatitude);

        return distance;
    }

    private String getRoadPointsData() {
        StringBuilder roadData = new StringBuilder();
        roadData.append(ROAD_POINTS_FILE_VERSION + ' ');
        for (RoadPoint roadPoint : roadPoints.values()) {
            if (!roadPoint.shouldBeSaved())
                continue;

            roadData.append(roadPoint.getLocation().getLatitude());
            roadData.append(' ');
            roadData.append(roadPoint.getLocation().getLongitude());
            roadData.append(' ');
            roadData.append(roadPoint.getLastTraveled().getTime());
            roadData.append(' ');

            ArrayList<RoadSegment> connections = roadPoint.getConnections();
            roadData.append(connections.size());
            roadData.append(' ');
            for (RoadSegment connection : connections) {
                GeoPoint connectionEndPoint = connection.getOtherEnd(roadPoint).getLocation();
                roadData.append(connectionEndPoint.getLatitude());
                roadData.append(' ');
                roadData.append(connectionEndPoint.getLongitude());
                roadData.append(' ');
            }
        }

        return roadData.toString();
    }

    private boolean hasBeenFetched(BoundingBox areaToCheck) {
        for (BoundingBox fetchedArea : fetchedAreas) {
            if (isInside(areaToCheck, fetchedArea))
                return true;
        }

        return false;
    }

    private void insertStartPoint(RoadPoint newPoint, RoadSegment roadSegment) {
        RoadPoint start = roadSegment.getFirstPoint();
        RoadPoint end = roadSegment.getSecondPoint();
        start.deleteConnection(end);
        end.deleteConnection(start);
        start.addConnection(newPoint);
        end.addConnection(newPoint);
        newPoint.addConnection(start);
        newPoint.addConnection(end);
        if (startPoint != null)
            deleteStartPoint();

        startPoint = newPoint;
    }

    private boolean isInside(BoundingBox insideArea, BoundingBox outsideArea) {
        boolean isInside = outsideArea.getLatNorth() >= insideArea.getLatNorth();
        isInside = isInside && (outsideArea.getLonEast() >= insideArea.getLonEast());
        isInside = isInside && (outsideArea.getLatSouth() <= insideArea.getLatSouth());
        return isInside && (outsideArea.getLonWest() <= insideArea.getLonWest());
    }

    private void loadBlockedRoads() {
        displayToast("Loading blocked roads.");
        File file = new File(context.getFilesDir(), BLOCKED_ROADS_FILE);
        if (!file.exists())
            return;

        try {
            Scanner scan = new Scanner(file);
            scan.useLocale(Locale.US);
            parseBlockedRoadsFile(scan);
            scan.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        displayToast("Finished loading blocked roads.");
    }

    private void loadRoadData() {
        loadBlockedRoads();
        loadRoadPoints();
    }

    private void loadRoadPoints() {
        displayToast("Loading road points.");
        File file = new File(context.getCacheDir(), ROAD_POINTS_FILE);
        if (!file.exists())
            return;

        try {
            Scanner scan = new Scanner(file);
            scan.useLocale(Locale.US);
            parseRoadPointsFile(scan);
            scan.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        displayToast("Finished loading road points.");
    }

    private void parseBlockedRoadsFile(Scanner scan) {
        if (scan.hasNext())
            if (!scan.next().equals(BLOCKED_ROADS_FILE_VERSION))
                return;

        while (scan.hasNextDouble()) {
            GeoPoint segmentStart = new GeoPoint(scan.nextDouble(), scan.nextDouble());
            GeoPoint segmentEnd = new GeoPoint(scan.nextDouble(), scan.nextDouble());
            RoadPoint roadPoint = updateRoadPoint(segmentStart);
            roadPoint.addConnection(updateRoadPoint(segmentEnd)).setBlocked(true);
        }
    }

    private void parseRoadPointsFile(Scanner scan) {
        if (scan.hasNext())
            if (!scan.next().equals(ROAD_POINTS_FILE_VERSION))
                return;

        while (scan.hasNextDouble()) {
            GeoPoint location = new GeoPoint(scan.nextDouble(), scan.nextDouble());
            Date lastTraveledTime = new Date(scan.nextLong());
            int numberOfConnections = scan.nextInt();
            ArrayList<RoadPoint> connections = new ArrayList<>();
            for (int connectionIndex = 0; connectionIndex < numberOfConnections; ++connectionIndex) {
                GeoPoint connectionLocation = new GeoPoint(scan.nextDouble(), scan.nextDouble());
                connections.add(updateRoadPoint(connectionLocation));
            }

            updateRoadPoint(location, lastTraveledTime, connections);
        }
    }

    // Removes fetched areas that area inside larger fetched areas.
    private void removeInsideAreas(BoundingBox newArea) {
        ArrayList<BoundingBox> newFetchedAreas = new ArrayList<>();
        for (BoundingBox fetchedArea : fetchedAreas) {
            if (!isInside(fetchedArea, newArea))
                newFetchedAreas.add(fetchedArea);
        }

        fetchedAreas = newFetchedAreas;
    }

    private void saveBlockedRoads() {
        displayToast("Saving blocked roads.");
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(BLOCKED_ROADS_FILE, Context.MODE_PRIVATE);
            outputStream.write(getBlockedRoadsData().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        displayToast("Finished saving blocked roads.");
    }

    private void updateRoadPoint(GeoPoint location, Date lastTraveledTime, ArrayList<RoadPoint> connections) {
        RoadPoint roadPoint = updateRoadPoint(location);
        roadPoint.setLastTraveled(lastTraveledTime);
        for (RoadPoint connection : connections) {
            roadPoint.addConnection(connection);
        }
    }

    private RoadPoint updateRoadPoint(GeoPoint location) {
        if (roadPoints.containsKey(location))
            return roadPoints.get(location);
        else {
            RoadPoint newRoadPoint = new RoadPoint(location);
            roadPoints.put(location, newRoadPoint);
            return newRoadPoint;
        }
    }

    public interface FinishedLoadingListener {
        void finishedLoadingEvent();
    }

    class LoadRoadsThread extends Thread {
        final FinishedLoadingListener finishedLoadingListener;

        LoadRoadsThread(FinishedLoadingListener finishedLoadingListener) {
            this.finishedLoadingListener = finishedLoadingListener;
        }

        public void run() {
            RoadManger.this.loadRoadData();
            if (finishedLoadingListener != null)
                finishedLoadingListener.finishedLoadingEvent();
        }
    }
}
