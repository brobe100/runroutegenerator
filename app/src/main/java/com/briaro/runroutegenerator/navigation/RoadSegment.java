package com.briaro.runroutegenerator.navigation;

class RoadSegment {
    private final RoadPoint firstPoint;
    private final RoadPoint secondPoint;
    private Double lengthMeters = null;
    private boolean isBlocked = false;

    RoadSegment(RoadPoint start, RoadPoint end) {
        this.firstPoint = start;
        this.secondPoint = end;
    }

    public Double getLengthMeters() {
        if (lengthMeters == null) {
            lengthMeters = firstPoint.getDistanceTo(secondPoint);
        }

        return lengthMeters;
    }

    public RoadPoint getFirstPoint() {
        return firstPoint;
    }

    public RoadPoint getSecondPoint() {
        return secondPoint;
    }

    public RoadPoint getOtherEnd(RoadPoint currentSegmentEnd) {
        if (firstPoint != currentSegmentEnd)
            return firstPoint;
        else
            return secondPoint;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
