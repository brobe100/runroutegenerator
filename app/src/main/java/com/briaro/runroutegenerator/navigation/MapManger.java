// This class managers everything with the map.

package com.briaro.runroutegenerator.navigation;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.briaro.runroutegenerator.R;

import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.CopyrightOverlay;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MapManger {
    private final MapView mapView;
    private final Context context;
    private final IMapController mapController;
    private final Bitmap currentPositionImage;
    private final Resources resources;
    private final RoadManger roadManger;
    private GeoPoint lastKnownLocation;
    private Marker currentLocationMarker = null;
    private Marker selectionMarker = null;
    private Marker startMarker = null;
    private Polyline overlayRoute;
    private FolderOverlay blockedRoadLines;

    public MapManger(Context context, MapView mapView, RoadManger roadManger) {
        this.context = context;
        resources = context.getResources();
        this.mapView = mapView;
        mapController = mapView.getController();
        this.roadManger = roadManger;
        currentPositionImage = getCurrentPositionImage();
        setUpMap();
        setupLocationListener();
        setupSensorListener();
        setupClickListener();
        drawBlockedRoads();
        mapView.invalidate();
    }

    public void onResume() {
        mapView.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause() {
        mapView.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    public void center() {
        mapController.setCenter(lastKnownLocation);
    }

    public void setStartMarker() {
        new SetStartMarker().start();
    }

    public GeoPoint getSelectorLocation() {
        if (selectionMarker == null)
            return null;

        return selectionMarker.getPosition();
    }

    public void drawBlockedRoads() {
        mapView.getOverlays().remove(blockedRoadLines);
        blockedRoadLines = new FolderOverlay();

        for (RoadSegment roadSegment : roadManger.getBlockedSegments()) {
            List<GeoPoint> geoPoints = new ArrayList<>(2);
            geoPoints.add(roadSegment.getFirstPoint().getLocation());
            geoPoints.add(roadSegment.getSecondPoint().getLocation());
            Polyline blockedSegmentLine = new Polyline();
            blockedSegmentLine.setColor(Color.RED);
            blockedSegmentLine.setPoints(geoPoints);
            blockedRoadLines.add(blockedSegmentLine);
        }

        mapView.getOverlays().add(blockedRoadLines);
        mapView.invalidate();
    }

    public void drawRoute(ArrayList<RoadPoint> route) {
        List<GeoPoint> geoPoints = new ArrayList<>();
        for (RoadPoint roadPoint : route) {
            geoPoints.add(roadPoint.getLocation());
        }

        if (overlayRoute == null) {
            overlayRoute = new Polyline();
            mapView.getOverlays().add(overlayRoute);
        }

        overlayRoute.setPoints(geoPoints);
        mapView.invalidate();
    }

    public GeoPoint getStartLocation() {
        if (startMarker != null)
            return startMarker.getPosition();

        if (selectionMarker != null)
            return selectionMarker.getPosition();

        if (currentLocationMarker != null)
            return currentLocationMarker.getPosition();

        return null;
    }

    private void drawCurrentPosition(GeoPoint location) {
        if (location == null)
            return;

        if (currentLocationMarker == null) {
            currentLocationMarker = new Marker(mapView);
            currentLocationMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);
            mapView.getOverlays().add(currentLocationMarker);
        }

        currentLocationMarker.setPosition(location);
        mapView.invalidate();
    }

    private Bitmap getCurrentPositionImage() {
        int positionResource = R.drawable.current_position;
        Bitmap positionBitmap = BitmapFactory.decodeResource(resources, positionResource);
        int newWidth = 60;
        int newHeight = 80;
        return Bitmap.createScaledBitmap(positionBitmap, newWidth, newHeight, false);
    }

    private Marker getSelectionMarker() {
        Marker selectionMarker = new Marker(mapView);
        int selectionMarkerResource = R.drawable.selection_marker;
        Bitmap selectionMarkerBitmap = BitmapFactory.decodeResource(resources, selectionMarkerResource);
        selectionMarker.setIcon(new BitmapDrawable(resources, selectionMarkerBitmap));
        selectionMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);
        return selectionMarker;
    }

    private Marker getStartMarker() {
        Marker startMarker = new Marker(mapView);
        Bitmap startMarkerBitmap = BitmapFactory.decodeResource(resources, R.drawable.start_marker);
        startMarker.setIcon(new BitmapDrawable(resources, startMarkerBitmap));
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        return startMarker;
    }

    private void setUpMap() {
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapController.setZoom(18.0);
        mapView.getOverlays().add(new CopyrightOverlay(context));
    }

    private void setupClickListener() {
        MapClickReceiver clickReceiver = new MapClickReceiver();
        MapEventsOverlay OverlayEvents = new MapEventsOverlay(clickReceiver);
        mapView.getOverlays().add(OverlayEvents);
    }

    private void setupLocationListener() {
        // Get location permissions.
        String accessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
        int fineLocationPermission = ActivityCompat.checkSelfPermission(context, accessFineLocation);
        boolean hasFinePermission = fineLocationPermission == PackageManager.PERMISSION_GRANTED;
        String accessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
        int courseLocationPermission = ActivityCompat.checkSelfPermission(context, accessCourseLocation);
        boolean hasCoursePermission = courseLocationPermission == PackageManager.PERMISSION_GRANTED;
        if (!hasFinePermission || !hasCoursePermission) {
            String[] permissions = {accessFineLocation, accessCourseLocation};
            ActivityCompat.requestPermissions((Activity) context, permissions, 0);
        }

        // Draw current position.
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Objects.requireNonNull(locationManager);
        String gpsProvider = LocationManager.GPS_PROVIDER;
        Location location = locationManager.getLastKnownLocation(gpsProvider);
        lastKnownLocation = new GeoPoint(location.getLatitude(), location.getLongitude());
        drawCurrentPosition(lastKnownLocation);
        mapController.setCenter(lastKnownLocation);

        // Set up location listener.
        MapManger.MyLocationListener locationListener = new MapManger.MyLocationListener();
        locationManager.requestLocationUpdates(gpsProvider, 0, 0, locationListener);
    }

    private void setupSensorListener() {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = Objects.requireNonNull(sensorManager).getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        MapManger.MySensorEventListener sensorListener = new MapManger.MySensorEventListener();
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(sensorListener, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    private void updateCurrentLocationMarker(float directionDegrees) {
        if (currentLocationMarker == null)
            return;

        Matrix matrix = new Matrix();
        matrix.postRotate(directionDegrees);
        Bitmap rotatedBitmap = Bitmap.createBitmap(currentPositionImage, 0, 0, currentPositionImage.getWidth(), currentPositionImage.getHeight(), matrix, true);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, rotatedBitmap);
        currentLocationMarker.setIcon(bitmapDrawable);
    }

    class SetStartMarker extends Thread {
        public void run() {
            if (startMarker == null) {
                startMarker = getStartMarker();
                mapView.getOverlays().add(startMarker);
            }

            if (selectionMarker != null) {
                startMarker.setPosition(selectionMarker.getPosition());
                mapView.invalidate();
            } else if (lastKnownLocation != null) {
                roadManger.fetchRoads(lastKnownLocation, 160);
                GeoPoint closestRoadPoint = roadManger.getClosestPointOnRoad(lastKnownLocation);
                if (closestRoadPoint == null)
                    return;

                startMarker.setPosition(closestRoadPoint);
                mapView.invalidate();
            }
        }
    }

    class SelectionMarkerDrawer extends Thread {
        final GeoPoint location;

        SelectionMarkerDrawer(GeoPoint location) {
            this.location = location;
        }

        public void run() {
            if (selectionMarker == null) {
                selectionMarker = getSelectionMarker();
                mapView.getOverlays().add(selectionMarker);
            }

            roadManger.fetchRoads(location, 160);
            GeoPoint closestRoadPoint = roadManger.getClosestPointOnRoad(location);
            if (closestRoadPoint == null)
                return;

            selectionMarker.setPosition(closestRoadPoint);
            mapView.invalidate();
        }
    }

    class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            lastKnownLocation = new GeoPoint(location.getLatitude(), location.getLongitude());
            drawCurrentPosition(lastKnownLocation);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    }

    class MySensorEventListener implements SensorEventListener {
        float[] mGravity;
        float[] mGeomagnetic;

        @Override
        public void onSensorChanged(SensorEvent event) {
            updateCurrentLocationMarker(360 - getOrientationDegrees(event));
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        private float getOrientationDegrees(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                mGravity = event.values;

            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                mGeomagnetic = event.values;

            if (mGravity != null && mGeomagnetic != null) {
                float R[] = new float[9];
                float I[] = new float[9];
                if (SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic)) {
                    // orientation contains azimuth, pitch and roll
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);
                    float azimuth = orientation[0];
                    return -azimuth * 360 / (2 * 3.14159f);
                }
            }

            return 0;
        }
    }

    class MapClickReceiver implements MapEventsReceiver {
        @Override
        public boolean singleTapConfirmedHelper(GeoPoint point) {
            new SelectionMarkerDrawer(point).start();
            return false;
        }

        @Override
        public boolean longPressHelper(GeoPoint point) {
            return false;
        }
    }
}
