package com.briaro.runroutegenerator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.briaro.runroutegenerator.navigation.MapManger;
import com.briaro.runroutegenerator.navigation.RoadManger;
import com.briaro.runroutegenerator.navigation.RoadPoint;
import com.briaro.runroutegenerator.navigation.RouteGenerator;

import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends Activity {
    private final View.OnClickListener blockRoadListener = new BlockRoadListener();
    private final View.OnClickListener unblockRoadListener = new UnblockRoadListener();
    private final View.OnClickListener routeButtonListener = new RouteButtonListener();
    private final View.OnClickListener exportButtonListener = new ExportButtonListener();
    private final View.OnClickListener centerButtonListener = new CenterButtonListener();
    private final View.OnClickListener setStartListener = new SetStartListener();
    private final TextWatcher routeDistanceWatcher = new RouteDistanceWatcher();
    private MapManger mapManger;
    private RoadManger roadManger;
    private double routeDistanceMeters;
    private ArrayList<RoadPoint> route;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = this;

        // The load method sets the HTTP User Agent to your application's package name.  Abusing
        // osm's tile servers will get you banned based on this string
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Configuration.getInstance().load(context, sharedPreferences);
        setContentView(R.layout.activity_main);
        roadManger = new RoadManger(context, new FinishedLoadingListener());
        mapManger = new MapManger(context, (MapView) findViewById(R.id.map), roadManger);
        findViewById(R.id.blockRoadButton).setOnClickListener(blockRoadListener);
        findViewById(R.id.unblockRoadButton).setOnClickListener(unblockRoadListener);
        findViewById(R.id.centerButton).setOnClickListener(centerButtonListener);
        findViewById(R.id.setStartButton).setOnClickListener(setStartListener);
        findViewById(R.id.routeButton).setOnClickListener(routeButtonListener);
        findViewById(R.id.exportRouteButton).setOnClickListener(exportButtonListener);
        ((EditText) findViewById(R.id.routeDistance)).addTextChangedListener(routeDistanceWatcher);
    }

    public void onResume() {
        super.onResume();
        mapManger.onResume();
    }

    public void onPause() {
        super.onPause();
        mapManger.onPause();
    }

    private void blockRoadSegment(boolean block) {
        GeoPoint selectorLocation = mapManger.getSelectorLocation();
        if (selectorLocation != null) {
            roadManger.blockRoadSegment(selectorLocation, block);
            mapManger.drawBlockedRoads();
        }
    }

    class FinishedLoadingListener implements RoadManger.FinishedLoadingListener {
        @Override
        public void finishedLoadingEvent() {
            if (mapManger != null) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        mapManger.drawBlockedRoads();
                    }
                });
            }
        }
    }

    private class RouteDistanceWatcher implements TextWatcher {
        public void afterTextChanged(Editable test) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            double routeDistance;
            try {
                routeDistance = Double.parseDouble(s.toString());
                routeDistanceMeters = routeDistance * Constants.metersPerMile;
            } catch (NumberFormatException | NullPointerException e) {
                routeDistanceMeters = 0;
            }
        }
    }

    private class BlockRoadListener implements View.OnClickListener {
        public void onClick(View v) {
            blockRoadSegment(true);
        }
    }

    private class UnblockRoadListener implements View.OnClickListener {
        public void onClick(View v) {
            blockRoadSegment(false);
        }
    }

    private class CenterButtonListener implements View.OnClickListener {
        public void onClick(View v) {
            mapManger.center();
        }
    }

    private class SetStartListener implements View.OnClickListener {
        public void onClick(View v) {
            mapManger.setStartMarker();
        }
    }

    private class RouteButtonListener implements View.OnClickListener {
        public void onClick(View v) {
            new RouteGeneratorThread().start();
        }
    }

    class RouteGeneratorThread extends Thread {
        public void run() {
            GeoPoint startLocation = mapManger.getStartLocation();
            if (startLocation == null)
                return;

            Context mainActivityContext = MainActivity.this;
            RouteGenerator routeGenerator = new RouteGenerator(roadManger, mainActivityContext);
            route = routeGenerator.generateRoute(startLocation, routeDistanceMeters);
            mapManger.drawRoute(route);

            updateActualDistance(route);
        }

        private void updateActualDistance(ArrayList<RoadPoint> route) {
            Locale locale = Locale.getDefault();
            String format = "%.2f";
            double routedDistanceMeters = RouteGenerator.calculateRouteDistance(route);
            double routedDistanceMiles = routedDistanceMeters / Constants.metersPerMile;
            String routeDistanceString = String.format(locale, format, routedDistanceMiles);
            routeDistanceString += " miles";
            final String distanceStringFinal = routeDistanceString;
            Runnable updateActualDistance = new Runnable() {
                @Override
                public void run() {
                    ((TextView) findViewById(R.id.actualDistance)).setText(distanceStringFinal);
                }
            };

            runOnUiThread(updateActualDistance);
        }
    }

    private class ExportButtonListener implements View.OnClickListener {
        public void onClick(View v) {
            new ExportGpxThread().start();
        }
    }

    class ExportGpxThread extends Thread {
        public void run() {
            GpxGenerator.exportGpxFile(route, MainActivity.this);
        }
    }
}
