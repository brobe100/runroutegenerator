package com.briaro.runroutegenerator;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.briaro.runroutegenerator.navigation.RoadPoint;

import org.osmdroid.util.GeoPoint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

class GpxGenerator {
    static private final String datePattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSZ";
    static private final SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern, Locale.US);

    private static String generateGpxFile(ArrayList<RoadPoint> route) {
        String gpx = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<gpx creator=\"Run Route Generator\" version=\"1.1\"\n" +
                "  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd\"\n" +
                "  xmlns:ns3=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\n" +
                "  xmlns=\"http://www.topografix.com/GPX/1/1\"\n" +
                "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ns2=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\">\n" +
                "  <metadata>\n" +
                "    <name>Test route</name>\n" +
                "    <time>" + dateFormatter.format(new Date()) + "</time>\n" +
                "  </metadata>\n" +
                "  <trk>\n" +
                "    <trkseg>\n";

        gpx += generateTrackPoints(route);
        gpx += "    </trkseg>\n" +
                "  </trk>\n" +
                "</gpx>\n";

        return gpx;
    }

    private static String generateTrackPoints(ArrayList<RoadPoint> route) {
        StringBuilder trkpts = new StringBuilder();
        long timeMilliseconds = new Date().getTime();
        int timeOffset = 0;
        for (RoadPoint roadPoint : route) {
            trkpts.append("      <trkpt lat=\"");
            GeoPoint location = roadPoint.getLocation();
            trkpts.append(Double.toString(location.getLatitude()));
            trkpts.append("\" lon=\"");
            trkpts.append(Double.toString(location.getLongitude()));
            trkpts.append("\">");
            trkpts.append("        <time>");
            trkpts.append(dateFormatter.format(new Date(timeMilliseconds + timeOffset++)));
            trkpts.append("</time>");
            trkpts.append("      </trkpt>\n");
        }

        return trkpts.toString();
    }

    public static void exportGpxFile(ArrayList<RoadPoint> route, Context context) {
        if (route == null) {
            displayToast(context, "No route to write to file.");
            return;
        }

        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            displayToast(context, "Can't access external storage.");
            return;
        }

        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int externalStoragePermission = ContextCompat.checkSelfPermission(context, writeExternalStorage);
        if (externalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            Activity activity = (Activity) context;
            String[] requestedPermissions = {writeExternalStorage};
            ActivityCompat.requestPermissions(activity, requestedPermissions, 0);
        } else
            writeGpxFile(context, route);
    }

    private static void writeGpxFile(Context context, ArrayList<RoadPoint> route) {
        File path = new File(Environment.getExternalStorageDirectory(), "RunRouteGenerator");
        if (!path.exists())
            if (!path.mkdir())
                displayToast(context, "Could not create directory.");

        File file = new File(path, "testgpx.gpx");
        FileOutputStream stream;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            displayToast(context, "Error creating file.");
            return;
        }

        try {
            stream.write(GpxGenerator.generateGpxFile(route).getBytes());
        } catch (IOException e) {
            displayToast(context, "Error writing file.");
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                displayToast(context, "Failed to close file.");
            }
        }

        displayToast(context, "File written to " + file.getPath());
    }

    private static void displayToast(final Context context, final String message) {
        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
